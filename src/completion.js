const  vscode = require('vscode');
var  fs = require('fs');
function deepClone(obj) 
{
    //如果不是复杂数据类型,就直接返回一个一样的对象
    if(typeof obj !="object"){
        return obj
    }
    //如果是,就递归调用
    var newObj = {};
    for (var key in obj) {
        newObj[key] = deepClone(obj[key])
    }
    return newObj;
}
function rmRepeatedSpace(str)
{
    var  lineText_split_space = str.split(' ');str
    var lineText_rm_spaces="";
    for (let i = 0; i < lineText_split_space.length; i++) 
    {
        if(lineText_split_space[i]!="")
        {
            lineText_rm_spaces+=lineText_split_space[i]+" ";
        }
    }
    lineText_rm_spaces=lineText_rm_spaces.slice(0,lineText_rm_spaces.length-1);
    return lineText_rm_spaces;
}
function getSubsectionName(lineText)
{
    var name_subsection="";
    if(lineText.includes("subsection"))
    {
        var str_rmRepeatedSpace = rmRepeatedSpace(lineText);
        var split_subsection = str_rmRepeatedSpace.split("subsection ");
        name_subsection = split_subsection[1];
    }
    return name_subsection;
}
function isComment(lineText)
{
    if(rmRepeatedSpace(lineText)[0]=="#")
    {
        return true;
    }
    return false;
}
// 从当前行向上查看，如果第一个遇到的是subsection关键词，则位于subsection里面；如果第一个遇到的是end关键词，则位于subsection之外。返回值为-1，则表示没有再subsection里面；否则表示距离最近的subsection的名字所在的行数
function isInSubsection(alltext, row)
{
    row = row - 1; //不包括当前行
    var num_end=0, num_subsection=0;
    for(let i = row;i>=0;i--)
    {
        if(isComment(alltext[i])) //忽略注释行
        {
            continue;
        }
        if(alltext[i].includes("subsection"))
        {
            if(num_end>0) 
            {
                num_end = num_end-1; //如果已经遇到过end关键词，则配对消除一个end计数
            }else
            {
                return i; //如果第一个遇到subsection关键词，那么肯定位于此subsection里面
            }
        }else if(alltext[i].includes("end"))
        {
            num_end = num_end+1;
        }
    }
    return -1;
}
function getSubsectionIndexes(alltext, row)
{
    var subsectionIndexes = []
    row = row - 1; //不包括当前行
    var num_end=0, num_subsection=0;
    for(let i = row;i>=0;i--)
    {
        if(isComment(alltext[i])) //忽略注释行
        {
            continue;
        }
        if(alltext[i].includes("subsection"))
        {
            if(num_end>0) 
            {
                num_end = num_end-1; //如果已经遇到过end关键词，则配对消除一个end计数
            }else if(num_end==0)
            {
                subsectionIndexes.push(i);
                //console.log("subsectionName index: ",i);
            }else
            {
                return subsectionIndexes;
            }
        }else if(alltext[i].includes("end"))
        {
            num_end = num_end+1;
        }
    }
    return subsectionIndexes;
}
function provideCompletionItems(document, position, token, context) {
    // 1. 获取当前文档所有文字
    const {activeTextEditor} = vscode.window;
    if(!activeTextEditor)return;
    const allText = activeTextEditor.document.getText().split('\n');
    // 2. 光标位置：行号和列号
    var col = position.character;
    var row = position.line;
    // 3. 当前光标之前的文本
    var  line        = document.lineAt(position);
    var  lineText = line.text.substring(0, position.character).replace(/[\t\n]/g,"");
    // 4. 去掉多余的空格
    var lineText_rm_spaces = rmRepeatedSpace(lineText);
    //console.log(lineText_rm_spaces);
    var firstCommand = lineText_rm_spaces.split(' ')[0];
    var lastChar = lineText_rm_spaces.split(' ')[lineText_rm_spaces.split(' ').length-1];
    var afterEqualSign = false;
    if(lineText.includes("="))afterEqualSign=true;
    //console.log("最后一个字符", lastChar);
    // 判断第一个命令是不是适合激活自动提示的操作
    if(!(firstCommand=="set" || firstCommand=="subsection")) //如果第一个命令既不是set也不是subsection，则不激活自动提醒。因为prm文件总是以set或subsection开始的
    {
        return;
    }
    //console.log("第一个命令是：",firstCommand);
    // 8. 获取ASPECT的参数json文件
    var  extensionPath = vscode.extensions.getExtension ("Zhikui.vscode-aspect").extensionPath;
    var parFile_json = "parameters.json";
    var jsonFile_ASPECT_Par=extensionPath;
    // 根据workspace的配置值获取版本号
    const version_aspect = vscode.workspace.getConfiguration().get("aspect.version");
    if (version_aspect) {
        vscode.window.setStatusBarMessage(`Current aspect version: ${version_aspect}`);
    }
    //--------
    if(extensionPath.indexOf('/')==0)
        {
            jsonFile_ASPECT_Par = extensionPath+"/resources/parameters/"+version_aspect+"/"+parFile_json;
        }else
        {
            jsonFile_ASPECT_Par = extensionPath+"\\resources\\parameters\\"+version_aspect+"\\"+parFile_json;
        }
    if (!fs.existsSync(jsonFile_ASPECT_Par))
    {
        vscode.window.showErrorMessage('Parameter file not found:'+jsonFile_ASPECT_Par);
        return;
    }else{
        vscode.window.showInformationMessage('Selected parameter file: ' + jsonFile_ASPECT_Par);
    }
    // 6. 判断当前光标是否位于subsection里面
    var subsection_row = isInSubsection(allText,row);
    var subsectionIndexes = getSubsectionIndexes(allText,row);

    // 7. 根据光标所处的位置特征判断如何提示
    var name_subsection="";
    var content_par = require(jsonFile_ASPECT_Par);
    var  json_par = deepClone(content_par);
    if(subsectionIndexes.length>=0) //位于subsection里面
    {
        var json_tmp=deepClone(content_par);
        var num_subsections = subsectionIndexes.length;
        //console.log("共有",num_subsections,"个subsection");
        for(let i =num_subsections-1; i>=0;i--)
        {
            subsection_row = subsectionIndexes[i];
            name_subsection= getSubsectionName(allText[subsection_row]);
            var key_subsection=name_subsection; //.replace(/ /g,'_20');
            if(version_aspect=="2.3.0")key_subsection=name_subsection.replace(/ /g,'_20');
            //console.log("第",i,"个subsection key:"+key_subsection+".");
            json_tmp = deepClone(json_tmp[key_subsection]);
            //TODO: 对于内层的subsection，还需要找到父subsection的key才行！！！
            if(json_tmp==undefined){
                console.error("The subsection of "+name_subsection+" is not available in ASPECT, key is "+key_subsection);
                return;
            }
        }
        json_par = json_tmp;
    }
    // 8. 根据第一命名的不同而设置不同的提示信息
    var json_show = {};
    if(firstCommand=="set") //只显示那些没有子字典的键值
    {
        if(lastChar=='=' || afterEqualSign==true) //如果最后一个字符是等号（=），则意味着是用=激活的，应该提示set parameter的值的选择
        {
            var name_par = lineText_rm_spaces.split(' =')[0].split('set ')[1];
            var key_par = name_par; //.replace(/ /g,"_20");
            if(version_aspect=="2.3.0")key_par = name_par.replace(/ /g,"_20");
            if(json_par[key_par]==undefined)
            {
                if(subsection_row>=0) //在subsection里面
                {
                    console.error("Parameter of "+name_par+" doesn't exist in subsection of "+name_subsection);
                    return;
                }else //没有再subsection里面
                {
                    console.error("Global parameter of "+name_par+" doesn't exist");
                    return;
                }
                
            }
            if(json_par[key_par]['default_value']!="")
            {
                var key='value_default';
                json_show[key]={};
                json_show[key]['kind']=vscode.CompletionItemKind.Property;
                json_show[key]['label'] = ' '+json_par[key_par]['default_value'];
                json_show[key]['label2']={}; 
                json_show[key]['label2']['name']=json_par[key_par]['default_value'];
                json_show[key]['label2']['qualifier']="Default value";
                json_show[key]['label2']['type']="default value";
                json_show[key]['detail']="";
                json_show[key]['documentation']={};
                json_show[key]['documentation']['value']="";
                if(json_par[key_par]['documentation']!=undefined){
                    json_show[key]['documentation']['value'] = json_par[key_par]['documentation'];
                }
                json_show[key]['documentation']['language']="aspect";
            }
            // 从pattern_description中分析提取可选的参数
            if(json_par[key_par]['pattern_description']!=undefined)
            {
                var patterns=json_par[key_par]['pattern_description'];
                patterns=patterns.replace(']','').split('Selection ')[1].split('|')
                for(let i =0;i<patterns.length;i++)
                {
                    var key='value'+i.toString();
                    json_show[key]={};
                    json_show[key]['label'] = ' '+patterns[i];
                    json_show[key]['label2']={}; 
                    json_show[key]['label2']['name']=patterns[i];
                    json_show[key]['label2']['qualifier']=""; //Optional
                    json_show[key]['label2']['type']=""; //value
                    json_show[key]['detail']="";
                    json_show[key]['documentation']={};
                    json_show[key]['documentation']['value']="";
                    if(json_par[key_par]['documentation']!=undefined){
                        json_show[key]['documentation']['value'] = json_par[key_par]['documentation'];
                    }
                    json_show[key]['documentation']['language']="aspect";
                }
                //console.log(patterns);
            }

            //console.log("par name: ",name_par,key_par);

        }else  //显示set后面的parameter name的各种选项
        {
            var keys_obj = Object.keys(json_par);
            for(let i=0;i<keys_obj.length;i++){
                var key = keys_obj[i];
                if(json_par[key]['value']!=undefined){
                    json_show[key] = {}; //deepClone(json_par[key]); //其实没必要这个
                    json_show[key]['kind']=vscode.CompletionItemKind.Value;
                    json_show[key]['label'] = key; //.replace(/_20/g," ");
                    if(version_aspect=="2.3.0")json_show[key]['label'] = key.replace(/_20/g," ");
                    json_show[key]['label2']={}; 
                    json_show[key]['label2']['name']=json_show[key]['label'];
                    json_show[key]['label2']['qualifier']=""; //param
                    json_show[key]['label2']['type']=""; //argument
                    json_show[key]['detail']="";
                    if(json_par[key]['pattern_description']!=undefined){
                        json_show[key]['detail']=json_par[key]['pattern_description'];
                    }
                    json_show[key]['documentation']={};
                    json_show[key]['documentation']['value']="";
                    if(json_par[key]['documentation']!=undefined){
                        json_show[key]['documentation']['value'] = json_par[key]['documentation'];
                    }
                    json_show[key]['documentation']['language']="aspect";
                }
            }
        }
    }else if(firstCommand=="subsection"){
        var keys_obj = Object.keys(json_par);
        for(let i=0;i<keys_obj.length;i++){
            var key = keys_obj[i];
            if(json_par[key]['value']==undefined){
                json_show[key] = {}; //deepClone(json_par[key]);
                json_show[key]['kind']=vscode.CompletionItemKind.Method;
                json_show[key]['label'] = key; //.replace(/_20/g," ");
                if(version_aspect=="2.3.0")json_show[key]['label'] = key.replace(/_20/g," ");
                json_show[key]['label2']={}; 
                json_show[key]['label2']['name']=json_show[key]['label'];
                json_show[key]['label2']['qualifier']=""; //subsection
                json_show[key]['label2']['type']=""; //argument
                json_show[key]['detail']="";
                json_show[key]['documentation']={};
                json_show[key]['documentation']['value']="";
                json_show[key]['documentation']['language']="aspect";
            }
        }
    }else
    {
        return;
    }
    //console.log(Object.keys(json_show));
    //console.log("所处的subsection的行数:", subsection_row);
    // 输出信息：debug
    //console.log("当前光标的列数： ",col, "，行数：", row);
    //console.log("文本行数：",allText.length);
    //console.log("当前光标所在行的文字：",allText[row]);
    
    // 返回自动补全和自动提示信息
    var  jsonkeys = Object.keys(json_show);
    return jsonkeys.map(function(key){
        var items=deepClone(json_show[key]);
        return items;
    });
    //console.log("触发完成");
}

function resolveCompletionItem(item, token) {
    return null;
}

module.exports = function(context) {
    // 第一个参数必须是语言的名称，在package里面定义的：aspect
    context.subscriptions.push(vscode.languages.registerCompletionItemProvider('aspect', {
        provideCompletionItems,
        resolveCompletionItem
    }, " ","="));
};
